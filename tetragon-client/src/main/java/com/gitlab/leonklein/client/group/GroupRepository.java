package com.gitlab.leonklein.client.group;

import java.util.List;

public interface GroupRepository {
  void create(
      String name,
      List<String> permissions
  );

  void update(
      String name,
      List<String> permissions
  );

  Group find(String name);
}
