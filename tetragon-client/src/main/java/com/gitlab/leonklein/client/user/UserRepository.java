package com.gitlab.leonklein.client.user;

import java.util.List;

public interface UserRepository {
  /**
    Create a new User in database. Default Rank and default Permission are "Spieler"
    @parm name Set the Name of the Player
    @parm uniqueId Set the UUID of the Playe
    @parm rank The default Rank of the Player: "Spieler"
    @parm permissions Set the default Permission of the Player. Comparison with Client-Permission
  */
  void create(
      String playerName,
      String uniqueId,
      String rank,
      List<String> permissions
  );

  void update(
      User user,
      String uniqueId,
      String rank,
      List<String> permissions
  );

  User find(String uniqueId);
}
