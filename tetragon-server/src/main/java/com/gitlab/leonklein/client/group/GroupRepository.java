package com.gitlab.leonklein.client.group;

import java.util.List;
import java.util.Optional;

public interface GroupRepository {
  void create(Group group);

  void update(Group group);

  Optional<Group> find(String name);

  List<Group> list();
}
