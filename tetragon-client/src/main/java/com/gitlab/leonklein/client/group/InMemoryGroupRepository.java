package com.gitlab.leonklein.client.group;

import com.gitlab.leonklein.client.group.GroupServiceGrpc.GroupServiceBlockingStub;
import java.util.List;

public class InMemoryGroupRepository implements GroupRepository {
  private final GroupServiceBlockingStub groupService;

  private InMemoryGroupRepository(GroupServiceBlockingStub groupService) {
    this.groupService =  groupService;
  }

  @Override
  public void create(
      String name,
      List<String> permissions
  ) {
    CreateGroupRequest request = createGroupRequest(name, permissions);
    CreateGroupResponse response = groupService.create(request);
    System.out.println("Group was Created: " + response.getGroup().getName());
  }

  private CreateGroupRequest createGroupRequest(
      String name,
      List<String> permissions
  ) {
    return CreateGroupRequest.newBuilder()
        .setGroup(createGroup(name, permissions))
        .build();
  }

  public Group createGroup(
      String name,
      List<String> permissions
  ) {
    return Group.newBuilder()
        .setName(name)
        .addAllPermissions(permissions)
        .build();
  }

  @Override
  public void update(
      String name,
      List<String> permissions
  ) {
    Group group = updateGroup(name, permissions);
    UpdateGroupRequest request = updateGroupRequest(group);
    UpdateGroupResponse response = groupService.update(request);
    System.out.println("Group was updated: " + response.getGroup().getName());
  }

  private UpdateGroupRequest updateGroupRequest(Group group) {
    return UpdateGroupRequest.newBuilder()
        .setGroup(group)
        .build();
  }

  private Group updateGroup(
      String name,
      List<String> permissions
  ) {
    return Group.newBuilder()
        .setName(name)
        .addAllPermissions(permissions)
        .build();
  }

  @Override
  public Group find(String name) {
    FindGroupRequest request = findGroupRequest(name);
    FindGroupResponse response = groupService.find(request);
    return response.getGroup();
  }

  private FindGroupRequest findGroupRequest(String name) {
    return FindGroupRequest.newBuilder()
        .setName(name)
        .build();
  }

  public static InMemoryGroupRepository create(GroupServiceBlockingStub groupService) {
    return new InMemoryGroupRepository(groupService);
  }
}
