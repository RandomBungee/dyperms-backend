package com.gitlab.leonklein.client.user;

import com.gitlab.leonklein.client.mysql.Mysql;
import io.grpc.stub.StreamObserver;
import java.util.Arrays;
import java.util.Optional;

public class UserService extends UserServiceGrpc.UserServiceImplBase {
  private final UserRepository userRepository;

  private UserService() {
    this.userRepository = SqlUserRepository.create(Mysql.connection);
  }

  @Override
  public void create(
      CreateUserRequest request,
      StreamObserver<CreateUserResponse> responseObserver
  ) {
    User user = request.getUser();
    userRepository.create(user);
    CreateUserResponse response = createUserResponse(user);
    responseObserver.onNext(response);
    responseObserver.onCompleted();
  }

  private CreateUserResponse createUserResponse(User user) {
    return CreateUserResponse.newBuilder()
        .setUser(user)
        .build();
  }

  @Override
  public void update(
      UpdateUserRequest request,
      StreamObserver<UpdateUserResponse> responseObserver
  ) {
    User user = request.getUser();
    userRepository.update(user);
    UpdateUserResponse response = updateUserResponse(user);
    responseObserver.onNext(response);
    responseObserver.onCompleted();
  }

  private UpdateUserResponse updateUserResponse(User user) {
    return UpdateUserResponse.newBuilder()
        .setUser(user)
        .build();
  }

  @Override
  public void find(
      FindUserRequest request,
      StreamObserver<FindUserResponse> responseObserver
  ) {
    String uniqueId = request.getUniqueId();
    Optional<User> optionalUser = userRepository.find(uniqueId);
    User user = optionalUser.orElse(noSuchUser());
    FindUserResponse response = findUserResponse(user);
    responseObserver.onNext(response);
    responseObserver.onCompleted();
  }

  private FindUserResponse findUserResponse(User user) {
    return FindUserResponse.newBuilder()
        .setUser(user)
        .build();
  }

  private User noSuchUser() {
    return User.newBuilder()
        .setName("")
        .setUniqueId("")
        .setRank("")
        .addAllPermission(Arrays.asList("", ""))
        .build();
  }

  public static UserService create() {
    return new UserService();
  }
}
