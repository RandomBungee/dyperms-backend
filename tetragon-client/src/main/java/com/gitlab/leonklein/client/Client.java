package com.gitlab.leonklein.client;

import com.gitlab.leonklein.client.group.GroupRepository;
import com.gitlab.leonklein.client.group.GroupServiceGrpc;
import com.gitlab.leonklein.client.group.InMemoryGroupRepository;
import com.gitlab.leonklein.client.user.InMemoryUserRepository;
import com.gitlab.leonklein.client.user.UserRepository;
import com.gitlab.leonklein.client.user.UserServiceGrpc;
import io.grpc.ManagedChannel;
import io.grpc.okhttp.OkHttpChannelBuilder;

public class Client {
  private static final String GRPC_ADRESS = "127.0.0.1";
  private static final int GRPC_PORT = 8891;
  private static GroupRepository groupRepository;
  private static UserRepository userRepository;

  public Client() {
    ManagedChannel managedChannel = createManagedChannel();
    groupRepository
        = InMemoryGroupRepository.create(GroupServiceGrpc.newBlockingStub(managedChannel));
    userRepository
        = InMemoryUserRepository.create(UserServiceGrpc.newBlockingStub(managedChannel));
  }

  public static void main(String[] arguments) {
    System.out.println("Client was started!");
  }

  public static ManagedChannel createManagedChannel() {
    return OkHttpChannelBuilder.forAddress(GRPC_ADRESS, GRPC_PORT)
        .usePlaintext()
        .build();
  }
}
