package com.gitlab.leonklein.client;

import com.gitlab.leonklein.client.group.GroupService;
import com.gitlab.leonklein.client.mysql.Mysql;
import com.gitlab.leonklein.client.user.UserService;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import java.io.IOException;

public class TetragonApp {
  private TetragonApp() {}

  private static final int GRPC_PORT = 8891;

  public static void main(String[] arguments)
      throws InterruptedException, IOException {
    init();
    Server server = ServerBuilder.forPort(GRPC_PORT)
        .addService(GroupService.create())
        .addService(UserService.create())
        .build();
    server.start();
    server.awaitTermination();
  }

  private static void init() {
    Mysql mysql = new Mysql(
        "localhost",
        "testbd123",
        "123456",
        "testbd123",
        3306
    );
    mysql.createTable();
  }
}
