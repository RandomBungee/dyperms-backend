package com.gitlab.leonklein.client.user;

import java.util.Optional;

public interface UserRepository {
  void create(User user);

  void update(User user);

  Optional<User> find(String uniqueId);
}
