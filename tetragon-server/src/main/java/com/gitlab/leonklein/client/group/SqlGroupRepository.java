package com.gitlab.leonklein.client.group;

import com.google.common.base.Preconditions;
import com.google.protobuf.ProtocolStringList;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public final class SqlGroupRepository implements GroupRepository {
  private final Connection connection;
  private final List<Group> groups;

  private SqlGroupRepository(Connection connection) {
    this.connection = connection;
    this.groups = new ArrayList<>();
  }

  @Override
  public void create(Group group) {
    try {
      PreparedStatement preparedStatement =
          connection.prepareStatement("INSERT INTO dyeperms_group (group_name,permission) VALUES (?,?)");
      groups.add(group);
      insertStatement(preparedStatement, group);
    } catch (SQLException createStatementFail) {
      System.err.println("Can´t create Group: " + createStatementFail.getMessage());
    }
  }

  private void insertStatement(
      PreparedStatement preparedStatement,
      Group group
  ) throws SQLException {
    Preconditions.checkNotNull(preparedStatement);
    Preconditions.checkNotNull(group);
    preparedStatement.setString(1, group.getName());
    preparedStatement.setString(2, getPermissionFromGroup(group));
    updateAndCloseStatement(preparedStatement);
  }

  @Override
  public void update(Group group) {
    try {
      PreparedStatement preparedStatement =
          connection.prepareStatement("UPDATE dyeperms_group SET permission = ? WHERE group_name = ?");
      updateStatement(preparedStatement, group);
    } catch (SQLException updateStatementFail) {
      System.err.println("Can´t update Group: " + updateStatementFail.getMessage());
    }
  }

  private void updateStatement(
      PreparedStatement preparedStatement,
      Group group
  ) throws SQLException {
    Preconditions.checkNotNull(preparedStatement);
    Preconditions.checkNotNull(group);
    preparedStatement.setString(1, getPermissionFromGroup(group));
    preparedStatement.setString(2, group.getName());
    updateAndCloseStatement(preparedStatement);
  }

  @Override
  public Optional<Group> find(String name) {
    try {
      PreparedStatement preparedStatement =
          connection.prepareStatement("SELECT * FROM dyeperms_group WHERE group_name = ?");
    } catch (SQLException findStatementFail) {
      System.err.println("Can´t find Group: " + findStatementFail.getMessage());
    }
    return Optional.empty();
  }

  private Optional<Group> findStatement(
      PreparedStatement preparedStatement,
      String name
  ) throws SQLException {
    preparedStatement.setString(1, name);
    ResultSet resultSet = preparedStatement.executeQuery();
    if(resultSet.next()) {
      String permissions = resultSet.getString("permission");
      Group group = Group.newBuilder()
          .setName(name)
          .addAllPermissions(Arrays.asList(permissions.split(", ")))
          .build();
      return Optional.of(group);
    }
    return Optional.empty();
  }

  @Override
  public List<Group> list() {
    return groups;
  }

  private void updateAndCloseStatement(PreparedStatement preparedStatement) throws SQLException {
    preparedStatement.executeUpdate();
    preparedStatement.close();
    System.out.println("Statement was updated and closed!");
  }

  private String getPermissionFromGroup(Group group) {
    ProtocolStringList permissionList = group.getPermissionsList();
    StringBuilder stringPermissionBuilder = new StringBuilder();
    for(String permissions : permissionList) {
      stringPermissionBuilder.append(permissions);
      stringPermissionBuilder.append(", ");
    }
    return stringPermissionBuilder.toString();
  }

  public static SqlGroupRepository create(Connection connection) {
    return new SqlGroupRepository(connection);
  }
}
