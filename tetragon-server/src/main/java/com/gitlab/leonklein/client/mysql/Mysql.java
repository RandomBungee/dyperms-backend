package com.gitlab.leonklein.client.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Mysql {
  private final String host;
  private final String user;
  private final String password;
  private final String database;
  private final int port;
  public static Connection connection;

  public Mysql(
      String host,
      String user,
      String password,
      String database,
      int port
  ) {
    this.host = host;
    this.user = user;
    this.password = password;
    this.database = database;
    this.port = port;
    connect();
  }

  private void connect() {
    try {
      connection =
          DriverManager.getConnection("jdbc://" + this.host + ":" + this.port + "/" + this.database + "?autoReconnect=true",
              this.user, this.password);
      System.out.println("MySQL was successfully connected!");
    } catch (SQLException connectionSetUpFail) {
      System.err.println("Can´t connect to MySQL: " + connectionSetUpFail.getMessage());
    }
  }

  public void createTable() {
    if(connection == null) { return; }
    try {
      connection.
          prepareStatement("CREATE TABLE IF NOT EXISTS dyeperms_group(group_name VARCHAR(100), permission TEXT)")
          .executeUpdate();
      connection.
          prepareStatement("CREATE TABLE IF NOT EXISTS dyeperms_user(user_name VARCHAR(16), unique_id VARCHAR(100), group VARCHAR(100), permission TEXT)")
          .executeUpdate();
    } catch (SQLException createTableFail) {
      System.err.println("Can´t create Table: " + createTableFail.getMessage());
    }
  }
}
