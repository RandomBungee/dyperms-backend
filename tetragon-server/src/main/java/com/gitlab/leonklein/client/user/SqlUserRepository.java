package com.gitlab.leonklein.client.user;

import com.google.protobuf.ProtocolStringList;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Optional;

public final class SqlUserRepository implements UserRepository {
  private final Connection connection;

  private SqlUserRepository(Connection connection) {
    this.connection = connection;
  }

  @Override
  public void create(User user) {
    try {
      PreparedStatement preparedStatement =
          connection.prepareStatement("INSERT INTO dyeperms_user (user_name,unique_id,group,permission) VALUES (?,?,?,?)");
      createStatement(preparedStatement, user);
    } catch (SQLException createStatementFail) {
      System.err.println("Can´t create User: " + createStatementFail.getMessage());
    }
  }

  private void createStatement(
      PreparedStatement preparedStatement,
      User user
  ) throws SQLException {
    preparedStatement.setString(1, user.getName());
    preparedStatement.setString(2, user.getUniqueId());
    preparedStatement.setString(3, user.getRank());
    preparedStatement.setString(4, getPermissionFromUser(user));
    updateAndCloseStatement(preparedStatement);
  }

  @Override
  public void update(User user) {
    try {
      PreparedStatement preparedStatement =
          connection.prepareStatement("UPDATE dyeperms_user SET group = ?, SET permission = ? WHERE unique_id");
      updateStatement(preparedStatement, user);
    } catch (SQLException updateStatementFail) {
      System.err.println("Can´t update User: " + updateStatementFail.getMessage());
    }
  }

  private void updateStatement(
      PreparedStatement preparedStatement,
      User user
  ) throws SQLException {
    preparedStatement.setString(1, user.getRank());
    preparedStatement.setString(2, getPermissionFromUser(user));
    preparedStatement.setString(3, user.getUniqueId());
    updateAndCloseStatement(preparedStatement);
  }

  @Override
  public Optional<User> find(String uniqueId) {
    try {
      PreparedStatement preparedStatement =
          connection.prepareStatement("SELECT * FROM dyeperms_user WHERE unique_id = ?");
    } catch (SQLException findStatementFail) {
      System.err.println("Can´t find Group: " + findStatementFail.getMessage());
    }
    return Optional.empty();
  }

  private Optional<User> findStatement(
      PreparedStatement preparedStatement,
      String uniqueId
  ) throws SQLException {
    preparedStatement.setString(1, uniqueId);
    ResultSet resultSet = preparedStatement.executeQuery();
    if(resultSet.next()) {
      String permissions = resultSet.getString("permission");
      User user = User.newBuilder()
          .setName(resultSet.getString("user_name"))
          .setUniqueId(uniqueId)
          .setRank("group")
          .addAllPermission(Arrays.asList(permissions.split(", ")))
          .build();
      return Optional.of(user);
    }
    return Optional.empty();
  }

  private String getPermissionFromUser(User user) {
    ProtocolStringList permissionList = user.getPermissionList();
    StringBuilder stringBuilder = new StringBuilder();
    for(String permission : permissionList) {
      stringBuilder.append(permission);
      stringBuilder.append(", ");
    }
    return stringBuilder.toString();
  }

  private void updateAndCloseStatement(PreparedStatement preparedStatement) throws SQLException {
    preparedStatement.executeUpdate();
    preparedStatement.close();
  }

  public static SqlUserRepository create(Connection connection) {
    return new SqlUserRepository(connection);
  }
}
