package com.gitlab.leonklein.client.user;

import com.gitlab.leonklein.client.user.UserServiceGrpc.UserServiceBlockingStub;
import java.util.List;

public class InMemoryUserRepository implements UserRepository {
  private final UserServiceBlockingStub userService;

  private InMemoryUserRepository(UserServiceBlockingStub userService) {
    this.userService = userService;
  }

  @Override
  public void create(
      String name,
      String uniqueId,
      String rank,
      List<String> permissions
  ) {
    User user = createUser(name, uniqueId, rank, permissions);
    CreateUserRequest request = createUserRequest(user);
    CreateUserResponse response = userService.create(request);
    System.out.println("User was created: " + response.getUser().getName());
  }

  private CreateUserRequest createUserRequest(User user) {
    return CreateUserRequest.newBuilder()
        .setUser(user)
        .build();
  }

  private User createUser(
      String name,
      String uniqueId,
      String rank,
      List<String> permissions
  ) {
    return User.newBuilder()
        .setName(name)
        .setUniqueId(uniqueId)
        .setRank(rank)
        .addAllPermission(permissions)
        .build();
  }

  @Override
  public void update(
      User user,
      String uniqueId,
      String rank,
      List<String> permissions
  ) {
    User defaultUser = find(uniqueId);
    UpdateUserRequest request = updateUserRequest(
        updateUser(user, rank, permissions));
    UpdateUserResponse response = userService.update(request);
    System.out.println("User was updated: " + response.getUser().getName());
  }

  private UpdateUserRequest updateUserRequest(User user) {
    return UpdateUserRequest.newBuilder()
        .setUser(user)
        .build();
  }

  private User updateUser(
      User user,
      String rank,
      List<String> permissions
  ) {
    return User.newBuilder()
        .setName(user.getName())
        .setUniqueId(user.getUniqueId())
        .setRank(rank)
        .addAllPermission(permissions)
        .build();
  }

  @Override
  public User find(String uniqueId) {
    FindUserRequest request = findUserRequest(uniqueId);
    FindUserResponse response = userService.find(request);
    return response.getUser();
  }

  private FindUserRequest findUserRequest(String uniqueId) {
    return FindUserRequest.newBuilder()
        .setUniqueId(uniqueId)
        .build();
  }

  public static InMemoryUserRepository create(UserServiceBlockingStub userService) {
    return new InMemoryUserRepository(userService);
  }
}
